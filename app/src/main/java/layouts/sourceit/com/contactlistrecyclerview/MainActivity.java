package layouts.sourceit.com.contactlistrecyclerview;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import layouts.sourceit.com.contactlistrecyclerview.model.User;

public class MainActivity extends AppCompatActivity implements Adapter.UserClickListener {

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        LinearLayoutManager ll = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(ll);

        Adapter adapter = new Adapter(this, generate(), this);
        recyclerView.setAdapter(adapter);

    }

    private List<User> generate() {
        List list = new ArrayList();
        for (int i = 0; i < 16; i++) {
            list.add(new User("Alex" + i, i + "alex@gmail.com", i + "address", "805251245" + i, i + R.drawable.contact1));
        }
        return list;
    }


    @Override
    public void onClick(User user) {
        Intent intent = new Intent(this, UserActivity.class);
        intent.putExtra("Key", user);
        startActivity(intent);
    }
}
