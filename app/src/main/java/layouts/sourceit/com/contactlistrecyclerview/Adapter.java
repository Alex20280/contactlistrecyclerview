package layouts.sourceit.com.contactlistrecyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import layouts.sourceit.com.contactlistrecyclerview.model.User;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    interface UserClickListener {
        void onClick(User user);
    }

    interface ItemClickListener {
        void onClick(int position);
    }

    private Context context;
    private List<User> list;
    private UserClickListener userClickListener;
    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onClick(int position) {
            userClickListener.onClick(list.get(position));
        }
    };


    public Adapter(Context context, List<User> list, UserClickListener userClickListener) {
        this.context = context;
        this.list = list;
        this.userClickListener = userClickListener;
    }

    @Override
    public Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(context).inflate(R.layout.item_view, parent, false);
        return new ViewHolder(item, itemClickListener);
    }

    @Override
    public void onBindViewHolder(Adapter.ViewHolder holder, int position) {
        User user = list.get(position);
        holder.name.setText(user.getName());
        holder.email.setText(user.getEmail());
        holder.image.setImageResource(user.getImages());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        EditText name;
        EditText email;
        ImageView image;
        ItemClickListener itemClickListener;

        public ViewHolder(View item, final ItemClickListener itemClickListener) {
            super(item);
            this.itemClickListener = itemClickListener;
            name = (EditText) item.findViewById(R.id.name);
            email = (EditText) item.findViewById(R.id.email);
            image = (ImageView) item.findViewById(R.id.image);//??
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onClick(getAdapterPosition());
                }
            });
        }
    }
}

