package layouts.sourceit.com.contactlistrecyclerview.model;

import android.location.Location;

import java.io.Serializable;

public class User implements Serializable {
    private String name;
    private String email;
    private String address;
    private String phone;
    private int images;


    public User(String name, String email, String address, String phone, int images) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.images = images;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public int getImages() {
        return images;
    }
}
