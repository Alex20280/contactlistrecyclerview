package layouts.sourceit.com.contactlistrecyclerview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import layouts.sourceit.com.contactlistrecyclerview.model.User;

public class UserActivity extends AppCompatActivity {

    TextView textViewName;
    TextView textViewEmail;
    TextView textViewAddress;
    TextView textViewPhone;
    TextView imageView;

    TextView detailedName;
    TextView detailedEmail;
    TextView detailedAddress;
    TextView detailedPhone;
    ImageView userActivityImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        textViewName = (TextView) findViewById(R.id.textViewName);
        textViewEmail = (TextView) findViewById(R.id.textViewEmail);
        textViewAddress = (TextView) findViewById(R.id.textViewAddress);
        textViewPhone = (TextView) findViewById(R.id.textViewPhone);
        imageView = (TextView) findViewById(R.id.imageView);

        detailedName = (TextView) findViewById(R.id.detailedName);
        detailedEmail = (TextView) findViewById(R.id.detailedEmail);
        detailedAddress = (TextView) findViewById(R.id.detailedAddress);
        detailedPhone = (TextView) findViewById(R.id.detailedPhone);
        userActivityImage = (ImageView) findViewById(R.id.userActivityImage);


        if (getIntent() != null) {
            User user = (User) getIntent().getSerializableExtra("Key");
            detailedName.setText(user.getName());
            detailedEmail.setText(user.getEmail());
            detailedAddress.setText(user.getAddress());
            detailedPhone.setText(user.getPhone());
            userActivityImage.setImageResource(user.getImages());

        }
    }
}
